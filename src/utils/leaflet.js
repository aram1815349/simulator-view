import L from 'leaflet'

export const createMarkerIcon = (path, size) => {
  return L.icon({
    iconUrl: path,
    iconSize: size, // size of the icon
    iconAnchor: [size[0] / 2, size[1]] // point of the icon which will correspond to marker's location
  })
}

export const createMarker = (coordinates, options) => {
  return L.marker(coordinates, options)
}
