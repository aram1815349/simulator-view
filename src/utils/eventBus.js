// eventBus.js
import { reactive } from 'vue';

const eventBus = {
  state: reactive({}),
  
  emit(event, data) {
    if (!this.state[event]) return;
    this.state[event].forEach(callback => callback(data));
  },

  on(event, callback) {
    if (!this.state[event]) {
      this.state[event] = [];
    }
    this.state[event].push(callback);
  },

  off(event, callback) {
    if (!this.state[event]) return;
    this.state[event] = this.state[event].filter(cb => cb !== callback);
  }
};

export default eventBus;
