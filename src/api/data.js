// src/api/data.js
export async function getMapData() {
  const endpointUrl = 'http://localhost:8081/simulation/getMap/1';

  try {
    const response = await fetch(endpointUrl);

    if (!response.ok) {
      throw new Error(`Erreur de requête: ${response.status}`);
    }

    const data = await response.json();
    return {
      sensorMap: data.sensorMap,
      fireMap: data.fireMap
    };
  } catch (error) {
    console.error(`Erreur lors de la récupération des données de la carte : ${error.message}`);
    throw error;
  }
}